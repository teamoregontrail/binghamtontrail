package hack.gbwin.binghamtontrail;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * Created by gbwin on 2/13/2016.
 */
public class MainPlayer implements Serializable{
    private String name;
    private int health; //out of 100
    private int stamina; //out of 100
    private int intelligence; //start low, and build
    private double money; //start low and build
    private int alcohol; //start low and aquire
    private String studentStatus; //what year/semester they are in
    private String currentLocation;
    private Hashtable cellPhone;
    //in-game have a "cell phone" with a list of names and the person object; relationship status will be displayed on screen

    public int maxHealth = 100; //out of 100
    public int maxStamina = 100; //out of 100

    public MainPlayer(String n){
        setName(n);
        health = 100;
        stamina = 100;
        intelligence = 10;
        money = 50.00;
        alcohol = 0;
        studentStatus = "freshman 1";
        currentLocation = "home";
        cellPhone = new Hashtable();
    }

    public MainPlayer(){
        setName("Tim Timson");
        health = 100;
        stamina = 100;
        intelligence = 10;
        money = 50.00;
        alcohol = 0;
        studentStatus = "freshman 1";
        currentLocation = "home";
        cellPhone = new Hashtable();
    }


    public String getName(){
        return name;
    }

    public void setName(String n){
        name = n;
    }

    public int getHealth(){
        return health;
    }

    public void setHealth(int num){
        health = num;
    }

    public void incrementHealth(int num){
        //max health is 100
        if(health >= 100){
            //can't go higher
            System.out.print("Health is already at 100%");
            health = 100;
        }
        else if(health <= 0){
            //can't go lower
            health = 0;
            System.out.print("You have died! Game Over.");
        }

        if(health < 10){
            System.out.print("Warning: You are very close to death!");
        }
    }

    public int getStamina(){
        return stamina;
    }

    public void setStamina(int num){
        stamina = num;
    }

    public void incrementStamina(int num){
        /*
        Stamina will start at 100, and every action costs stamina. If you reach zero you will pass out.
        Lazy events and sleeping will raise stamina. Everything else will lower it.
         */

        stamina += num;
        if(stamina >= 100){
            //can't go higher
            stamina = 100;
            System.out.print("Stamina is at 100%");
        }
        else if(stamina <= 0){
            //can't go lower
            stamina = 0;
            System.out.print("You have passed out from exhaustion! -10 health");
            //then decrease health
        }

        if (stamina < 10) {
            System.out.print("You are really tired man, you feel like you might pass out at any second.");
        } else if (stamina < 20) {
            System.out.print("You are very tired, and your eyelids are getting super heavy.");
        } else if (stamina < 30) {
            System.out.print("You are starting to get tired.");
        }
    }

    public int getIntelligence(){
        return intelligence;
    }

    public void setIntelligence(int num){
        intelligence = num;
    }

    public void incrementIntelligence(int num){
        /* the number will be either positive or negative
        Intelligence level will start at 10 at the beginning of the game;
        Going to class, working, etc. will make intelligence go up.
        Watching tv, excessive partying, drugs, etc. will make it go down.
         */
        intelligence += num;
        if(intelligence <= 0){
            //can't go lower
            intelligence = 0;
            System.out.print("You have become so stupid you've forgotten how to breathe. Good job dumbass you're dead.");
        }
    }

    public double getMoney(){
        return money;
    }

    public void setMoney(double m){
        money = m;
    }

    public int getAlcohol(){
        return alcohol;
    }

    public void setAlcohol(int num){
        alcohol = num;
    }

    public String getStudentStatus(){
        return studentStatus;
    }

    public void setStudentStatus(String status){
        studentStatus = status;
    }

    public String getCurrentLocation(){
        return currentLocation;
    }

    public void setCurrentLocation(String loc){
        currentLocation = loc;
    }

    public void setCellPhone(Person p){
        cellPhone.put(p.getName(),p);
    }

    public Hashtable getCellPhone(){
        return cellPhone;
    }


    public void playerToString(){
        System.out.println("Alcohol");
        System.out.println(getAlcohol());
        System.out.println("health");
        System.out.println(getHealth());
        System.out.println("location");
        System.out.println(getCurrentLocation());
        System.out.println("intel");
        System.out.println(getIntelligence());
        System.out.println("money");
        System.out.println(getMoney());
        System.out.println("name");
        System.out.println(getName());
        System.out.println("stamina");
        System.out.println(getStamina());
        System.out.println("status");
        System.out.println(getStudentStatus());
        System.out.println("DONE!!!");
        //System.out.println(getFriendsArray());
        //System.out.println(getProffessorsArray());
    }
}









