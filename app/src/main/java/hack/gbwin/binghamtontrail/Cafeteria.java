package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
public class Cafeteria extends Location {
    private String[] foodChoices = {"Stale Bread","Frozen Hot Pocket","Lava Hot Hot Pocket","Imitation meat","Mystery Meat"};

    public Cafeteria(){
        super("Cafeteria","Lunch Lady");
    }
    public String[] getFoods(){
        return foodChoices;
    }
}
