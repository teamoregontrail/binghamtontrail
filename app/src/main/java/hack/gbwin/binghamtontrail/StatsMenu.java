package hack.gbwin.binghamtontrail;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.io.Serializable;

public class StatsMenu extends AppCompatActivity implements Serializable{

        private MainPlayer player;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats_menu);
        Intent gameIntent = getIntent();
        player= (MainPlayer)gameIntent.getSerializableExtra("myObject");

        String nameString = "Your name is " + player.getName();
        TextView name= (TextView)findViewById(R.id.textName);
        name.setText(nameString);

        String healthString = "Your health is " + player.getHealth() + "/" + player.maxHealth;
        TextView health= (TextView)findViewById(R.id.textHealth);
        health.setText(healthString);

        String staminaString = "Your stamina is "+ player.getStamina() + "/" + player.maxStamina;
        TextView  stamina = (TextView)findViewById(R.id.textStamina);
        stamina.setText(staminaString);

        String intelligenceString = "You have " + player.getIntelligence() + " intelligence points!";
        TextView intelligence= (TextView)findViewById(R.id.textIntelligence);
        intelligence.setText(intelligenceString);

        String moneyString = "You have $" + player.getMoney();
        TextView money= (TextView)findViewById(R.id.textMoney);
        money.setText(moneyString);

        String alcoholString = "You have " + player.getAlcohol() + " alcohols!";
        TextView alcohol= (TextView)findViewById(R.id.textAlcohol);
        alcohol.setText(alcoholString);

        String statusString= "Your standing is " + player.getStudentStatus() + "!";
        TextView status= (TextView)findViewById(R.id.textYear);
        status.setText(statusString);

        String locationString = "Your location is " + player.getCurrentLocation() + "!";
        TextView location= (TextView)findViewById(R.id.textLocation);
        location.setText(locationString);

        String contactString = "You have " + player.getCellPhone().size() + " contacts!";
        TextView contact= (TextView)findViewById(R.id.textContacts);
        contact.setText(contactString);
    }
        public void update(MainPlayer p1){
            player = p1;
        }

}