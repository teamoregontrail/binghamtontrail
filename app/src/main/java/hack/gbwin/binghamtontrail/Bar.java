package hack.gbwin.binghamtontrail;

import java.util.Hashtable;

/**
 * Created by kevintravers on 2/13/16.
 */
public class Bar extends Location{

    private static String storeNames[] = {"The Mouse", "Bar of America","Uncle Sam's Tavern"};
    private static Hashtable storeItems = new Hashtable();
    public Bar(){
        super(getThisStoreName(), myLibrary.getNames());
        //setStoreName(getStoreNames());
        //setStoreClerkName(myLibrary.getNames());
        storeItems.put("Beer",new Double(20.00));
        storeItems.put("Long island ice tea",new Double(25.00));
        storeItems.put("Irish car bomb",new Double(30.00));
        storeItems.put("Mystery Punch",new Double(1.00));
        storeItems.put("Old Fashioned",new Double(35.00));
        storeItems.put("Rum and Coke",new Double(23.00));
        storeItems.put("Hot Pockets",new Double(2.00));
    }
    public static int getStoreNamesSize(){
        return storeNames.length;
    }
    public static String getThisStoreName(){
        return storeNames[myLibrary.randomInt(getStoreNamesSize(),0)];
    }
    public Hashtable getStoreItems() {
        return storeItems;
    }
    public void setStoreItems(Hashtable storeItems) {
        this.storeItems = storeItems;
    }
}
