package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
public class Home{
    //these can change when player lives off campus
    //number people can be in room at once
    private int roomCapacity;
    //amount of people live in a room
    private int maxRoomates = 4;
    //on campus or not
    private  boolean onCampus;
    //roomates currently living in room
    private int totalRoomates;
    //description of the room


    public Home(){
        this.totalRoomates = myLibrary.randomInt(maxRoomates, 1);

        this.roomCapacity = myLibrary.randomInt((totalRoomates * 4), (maxRoomates * 2));
        this.onCampus = true;
    }

    public int getRoomCapacity(){
        return roomCapacity;
    }
    public boolean getOnCampus(){
        return onCampus;
    }

    public int getTotalRoomates(){
        return totalRoomates;
    }
    public void setOnCampus(boolean newValue){
        onCampus = newValue;
    }
    public void setRoomCapacity(int newValue){
        roomCapacity =newValue;
    }
    public void setTotalRoomates(int newValue){
        totalRoomates = newValue;
    }



}

