package hack.gbwin.binghamtontrail;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Journey1 extends AppCompatActivity {

    private TextView textView;
    private Button button1, button2, button3, button4;
    private int state = 0;

    private MainPlayer player;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journey1);
        onStudyButtonPress();
        Intent myIntent = getIntent();
        player = (MainPlayer) myIntent.getSerializableExtra("myObject");
    }

    public void onStudyButtonPress() {
        textView = (TextView) findViewById(R.id.textView1);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);

        switch (state) {
            case 0:
                // No parent decision, automatically brought here
                // Choose between 1. Going to library 2. Speed dating
                textView.setText(R.string.case0text1);
                button1.setText(R.string.case0button1);
                button2.setText(R.string.case0button2);
                button3.setText(R.string.case0button3);
                button4.setText(R.string.case0button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 1;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 2;
                        onStudyButtonPress();
                    }
                });
                break;
            case 1:
                textView.setText(R.string.case1text1);
                button1.setText(R.string.case1button1);
                button2.setText(R.string.case1button2);
                button3.setText(R.string.case1button3);
                button4.setText(R.string.case1button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 4;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 3;
                        onStudyButtonPress();
                    }
                });
                break;
            case 2:
                textView.setText(R.string.case2text1);
                button1.setText(R.string.case2button1);
                button2.setText(R.string.case2button2);
                button3.setText(R.string.case2button3);
                button4.setText(R.string.case2button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 20;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 21;
                        onStudyButtonPress();
                    }
                });
                break;

            case 3:
                textView.setText(R.string.case3text1);
                button1.setText(R.string.case3button1);
                button2.setText(R.string.case3button2);
                button3.setText(R.string.case3button3);
                button4.setText(R.string.case3button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 5;
                        player.incrementIntelligence(10);
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 6;
                        onStudyButtonPress();
                    }
                });
                break;
            case 4:
                textView.setText(R.string.case4text1);
                button1.setText(R.string.case4button1);
                button2.setText(R.string.case4button2);
                button3.setText(R.string.case4button3);
                button4.setText(R.string.case4button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 5:
                textView.setText(R.string.case5text1);
                button1.setText(R.string.case5button1);
                button2.setText(R.string.case5button2);
                button3.setText(R.string.case5button3);
                button4.setText(R.string.case5button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 7;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 8;
                        onStudyButtonPress();
                    }
                });
                break;
            case 6:
                textView.setText(R.string.case6text1);
                button1.setText(R.string.case6button1);
                button2.setText(R.string.case6button2);
                button3.setText(R.string.case6button3);
                button4.setText(R.string.case6button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 7:
                textView.setText(R.string.case7text1);
                button1.setText(R.string.case7button1);
                button2.setText(R.string.case7button2);
                button3.setText(R.string.case7button3);
                button4.setText(R.string.case7button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 9;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 10;
                        onStudyButtonPress();
                    }
                });
                button3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 11;
                        onStudyButtonPress();
                    }
                });
                button4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 12;
                        onStudyButtonPress();
                    }
                });
                break;
            case 8:
                textView.setText(R.string.case8text1);
                button1.setText(R.string.case8button1);
                button2.setText(R.string.case8button2);
                button3.setText(R.string.case8button3);
                button4.setText(R.string.case8button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 9:
                textView.setText(R.string.case9text1);
                button1.setText(R.string.case9button1);
                button2.setText(R.string.case9button2);
                button3.setText(R.string.case9button3);
                button4.setText(R.string.case9button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 13;
                        onStudyButtonPress();
                    }
                });
                break;
            case 10:
                textView.setText(R.string.case10text1);
                button1.setText(R.string.case10button1);
                button2.setText(R.string.case10button2);
                button3.setText(R.string.case10button3);
                button4.setText(R.string.case10button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 11:
                textView.setText(R.string.case11text1);
                button1.setText(R.string.case11button1);
                button2.setText(R.string.case11button2);
                button3.setText(R.string.case11button3);
                button4.setText(R.string.case11button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 12:
                textView.setText(R.string.case12text1);
                button1.setText(R.string.case12button1);
                button2.setText(R.string.case12button2);
                button3.setText(R.string.case12button3);
                button4.setText(R.string.case12button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 13:
                textView.setText(R.string.case13text1);
                button1.setText(R.string.case13button1);
                button2.setText(R.string.case13button2);
                button3.setText(R.string.case13button3);
                button4.setText(R.string.case13button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                       state = 14;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                       state = 15;
                        onStudyButtonPress();
                    }
                });
                break;
            case 14:
                textView.setText(R.string.case14text1);
                button1.setText(R.string.case14button1);
                button2.setText(R.string.case14button2);
                button3.setText(R.string.case14button3);
                button4.setText(R.string.case14button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 15:
                textView.setText(R.string.case15text1);
                button1.setText(R.string.case15button1);
                button2.setText(R.string.case15button2);
                button3.setText(R.string.case15button3);
                button4.setText(R.string.case15button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 16;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 17;
                        onStudyButtonPress();
                    }
                });
                button3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 18;
                        onStudyButtonPress();
                    }
                });
                button4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 19;
                        onStudyButtonPress();
                    }
                });
                break;
            case 16:
                textView.setText(R.string.case16text1);
                button1.setText(R.string.case16button1);
                button2.setText(R.string.case16button2);
                button3.setText(R.string.case16button3);
                button4.setText(R.string.case16button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 17:
                textView.setText(R.string.case17text1);
                button1.setText(R.string.case17button1);
                button2.setText(R.string.case17button2);
                button3.setText(R.string.case17button3);
                button4.setText(R.string.case17button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 18:
                textView.setText(R.string.case18text1);
                button1.setText(R.string.case18button1);
                button2.setText(R.string.case18button2);
                button3.setText(R.string.case18button3);
                button4.setText(R.string.case18button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 19:
                textView.setText(R.string.case19text1);
                button1.setText(R.string.case19button1);
                button2.setText(R.string.case19button2);
                button3.setText(R.string.case19button3);
                button4.setText(R.string.case19button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 20:
                textView.setText(R.string.case20text1);
                button1.setText(R.string.case20button1);
                button2.setText(R.string.case20button2);
                button3.setText(R.string.case20button3);
                button4.setText(R.string.case20button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                       state = 22;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 23;
                        onStudyButtonPress();

                    }
                });
                break;
            case 21:
                textView.setText(R.string.case21text1);
                button1.setText(R.string.case21button1);
                button2.setText(R.string.case21button2);
                button3.setText(R.string.case21button3);
                button4.setText(R.string.case21button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 30;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                       state = 18;
                        onStudyButtonPress();
                    }
                });
                break;
            case 22:
                textView.setText(R.string.case22text1);
                button1.setText(R.string.case22button1);
                button2.setText(R.string.case22button2);
                button3.setText(R.string.case22button3);
                button4.setText(R.string.case22button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                    state = 24;
                        onStudyButtonPress();
                    }
                });
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 25;
                        onStudyButtonPress();
                    }
                });
                break;
            case 23:
                textView.setText(R.string.case23text1);
                button1.setText(R.string.case23button1);
                button2.setText(R.string.case23button2);
                button3.setText(R.string.case23button3);
                button4.setText(R.string.case23button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 24:
                textView.setText(R.string.case24text1);
                button1.setText(R.string.case24button1);
                button2.setText(R.string.case24button2);
                button3.setText(R.string.case24button3);
                button4.setText(R.string.case24button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 25:
                textView.setText(R.string.case25text1);
                button1.setText(R.string.case25button1);
                button2.setText(R.string.case25button2);
                button3.setText(R.string.case25button3);
                button4.setText(R.string.case25button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 26;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 27;
                        onStudyButtonPress();
                    }
                });
                break;
            case 26:
                textView.setText(R.string.case26text1);
                button1.setText(R.string.case26button1);
                button2.setText(R.string.case26button2);
                button3.setText(R.string.case26button3);
                button4.setText(R.string.case26button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 27:
                textView.setText(R.string.case27text1);
                button1.setText(R.string.case27button1);
                button2.setText(R.string.case27button2);
                button3.setText(R.string.case27button3);
                button4.setText(R.string.case27button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 28;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 29;
                        onStudyButtonPress();
                    }
                });
                break;

            case 28:
                textView.setText(R.string.case28text1);
                button1.setText(R.string.case28button1);
                button2.setText(R.string.case28button2);
                button3.setText(R.string.case28button3);
                button4.setText(R.string.case28button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 29:
                textView.setText(R.string.case29text1);
                button1.setText(R.string.case29button1);
                button2.setText(R.string.case29button2);
                button3.setText(R.string.case29button3);
                button4.setText(R.string.case29button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;
            case 30:
                textView.setText(R.string.case30text1);
                button1.setText(R.string.case30button1);
                button2.setText(R.string.case30button2);
                button3.setText(R.string.case30button3);
                button4.setText(R.string.case30button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 32;
                        onStudyButtonPress();
                    }
                });
                button2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        state = 22;
                        onStudyButtonPress();
                    }
                });
                break;

            case 32:
                textView.setText(R.string.case32text1);
                button1.setText(R.string.case32button1);
                button2.setText(R.string.case32button2);
                button3.setText(R.string.case32button3);
                button4.setText(R.string.case32button4);
                buttonAdjuster();
                button1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        resetApp();
                    }
                });
                break;

            default:
                System.out.println("How did you get here?");
                break;


        }
    }

        public void buttonAdjuster () {
            if (button2.getText().length() < 1) {
                button2.setVisibility(View.GONE);
                System.out.println("GONE");
            } else {
                button2.setVisibility(View.VISIBLE);
                System.out.println("VISIBLE");
            }
            if (button3.getText().length() < 1) {
                button3.setVisibility(View.GONE);
                System.out.println("GONE");
            } else {
                button3.setVisibility(View.VISIBLE);
                System.out.println("VISIBLE");
            }
            if (button4.getText().length() < 1) {
                button4.setVisibility(View.GONE);
                System.out.println("GONE");
            } else {
                button4.setVisibility(View.VISIBLE);
                System.out.println("VISIBLE");
            }

        }

        public void resetApp(){
            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
        }
//        final Button button1 = (Button) findViewById(R.id.button1);
        //button1.callOnClick();
        //button1.
    public void onStatsButton(View view) {
        Intent intent = new Intent(this, StatsMenu.class);
        intent.putExtra("myObject", player);
        startActivity(intent);
    }
}
