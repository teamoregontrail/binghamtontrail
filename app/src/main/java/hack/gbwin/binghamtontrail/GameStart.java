package hack.gbwin.binghamtontrail;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class GameStart extends AppCompatActivity {

    private MainPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_start);
        Intent myIntent = getIntent();
        player = (MainPlayer) myIntent.getSerializableExtra("myObject");

    }

    public void onYesButtonPress(View view) {
        Intent intent = new Intent(this, Journey1.class);
        intent.putExtra("myObject", player);
        startActivity(intent);
        finish();
    }
}
