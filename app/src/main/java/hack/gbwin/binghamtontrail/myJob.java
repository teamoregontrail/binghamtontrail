package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
public class myJob {
    private int totalEmployees;
    private int totalHours;
    private String jobTitle;
    private String jobDescription;
    private double salary;
    //store manager
    private Person manager;
    //0-100 odds of getting fired for poor perfomaance
    //bellow 30% increases chance of getting fired
    //above 80% increases cahnce of pay raise
    private int jobStatus;

    public myJob(){
        this.totalEmployees = 0;
        this.totalHours = 0;
        this.jobTitle = "unemployeed bum";
        this.jobDescription = "College Student";
        //parents giving money
        this.salary = 0;
        this.jobStatus = 0;
        this.manager = new Person();
    }
    public myJob(String newJobTitle, int newSalary){
        this.totalEmployees = 0;
        this.totalHours = 8;
        this.jobTitle = newJobTitle;
        this.jobDescription = "College Student";
        //parents giving money
        this.salary = newSalary;
        this.jobStatus = 50;
        this.manager = new Person();
    }

    public double getSalary(){
        return this.salary;
    }
    public Person getBoss(){
        return manager;
    }
    public void setBoss(Person newBoss){
        manager = newBoss;
    }
    public String getBossName(){
        return manager.getName();
    }
    public int getTotalHours(){
        return this.totalHours;
    }
    public String getJobTitle(){
        return this.jobTitle;
    }
    public String getJobDescription(){
        return this.jobDescription;
    }
    public int getJobStatus(){
        return this.jobStatus;
    }
    public void setJobStatus(int newValue){
        this.jobStatus = newValue;
    }
    public void setSalary(int newValue){
        this.salary = this.salary;
    }
    public void incrementSalary(int newValue){
        this.salary = this.salary + newValue;
    }
    public void setJobTitle(String newValue){
        this.jobTitle = newValue;
    }
    public void setJobDescription(String newValue){
        this.jobDescription = newValue;
    }
}
