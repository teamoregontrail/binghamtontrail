package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Hashtable;
import java.util.Vector;

public class myLibrary {
    private static String adjectives[] = {"rotten","nasty","breezy","magnificent","dangerous","adorable","sparkling","elegant"};
    private static ArrayList<String> names = new ArrayList<String>(Arrays.asList("Kevin","Matt","Gabby","Harry","Alex","Ashley","Sara","Dylan","Elaine","Faseeh","Rosana"));
    private static String[] nouns;
    private static ArrayList<String> profNames = new ArrayList<String>(Arrays.asList("Madden","Rose","Head","Smith","Garrison","I Forgot","Proffessor X","Chris Evans","Wade Wilson","Tony stark","beard Face"));

    //private Hashtable bookStore = new Hashtable();
    private static Hashtable bookStore = new Hashtable();


    public static Hashtable getBookStore() {
        bookStore.put("Science TextBook",new Double(150.00));
        bookStore.put("Math TextBook",new Double(350.00));
        bookStore.put("History TextBook",new Double(250.00));
        bookStore.put("Philosphy TextBook",new Double(650.00));
        return bookStore;
    }

    public static int randomInt(int max, int min){
        int result = min + (int)(Math.random() * ((max - min) + 1));
        return result;
    }
    public static String getNames(){
        int rand = randomInt(names.size(),0);
        String result = names.get(rand);
        names.remove(rand);
        return result;
    }
    public static String getProfNames(){
        return profNames.get(randomInt(profNames.size(),0));
    }
    public static String getAdjective(){
        return adjectives[randomInt(adjectives.length,0)];
    }


}

