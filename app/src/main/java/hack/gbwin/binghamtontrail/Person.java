package hack.gbwin.binghamtontrail;

import java.io.Serializable;

/**
 * Created by gbwin on 2/13/2016.
 */
public class Person implements Serializable{

    /*
    1-10 - This person is your Enemy
    11-20 - This person hates you
    21-40 - This person dislikes you
    41-60 - This person is Neutral about you
    61-80 - This person likes you
    81-90 - This person is your Friend
    91-100 - This person is your Best Friend
     */

    private int relationship;
    private String name;

    public int getRelationship(){
        return relationship;
    }

    public void setRelationship(int num){
        relationship = num;
    }

    public void incrementRelationship(int num){
        //the number will be either positive or negative
        relationship += num;
        if(relationship >= 100){
            //can't go higher
            relationship = 100;
        }
        else if(relationship <= 0){
            //can't go lower
            relationship = 0;
        }
    }

    public String getName(){
        return name;
    }
    public void setName(String n){
        name = n;
    }

    public Person(String n, int rel){
        setName(n);
        setRelationship(rel);
    }

    public Person(String n){
        setName(n);
        setRelationship(50);
    }
    public Person(){
        setName(myLibrary.getNames());
        setRelationship(myLibrary.randomInt(75,25));
    }

    //enemy will call cops on you if you invite them to your party
}
