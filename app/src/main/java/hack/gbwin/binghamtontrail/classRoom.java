package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
public class ClassRoom extends Location {
    private String subject;
    private Person professor;
    private String[] classes = {"Science","Math","History","English","Art","Philosphy"};
    public ClassRoom(String className){
        super(className,myLibrary.getProfNames());
        this.subject = className;
    }

    public ClassRoom(){
        super();
        professor = new Person(myLibrary.getProfNames());
        this.subject = classes[myLibrary.randomInt(classes.length,0)];
        //
    }
    public String getProfessorName(){
        return professor.getName();
    }
    public void setProfessor(Person newProfessor){
        this.professor = newProfessor;
    }
    public Person getProfessor(){
        return this.professor;
    }
    public String getSubject(){
        return this.subject;
    }
    public void setSubject(String subjectName){
        this.subject = subjectName;
    }
}
