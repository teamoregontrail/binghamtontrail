package hack.gbwin.binghamtontrail;

/**
 * Created by kevintravers on 2/13/16.
 */
public class Location {
    private String storeClerkName;
    private String storeName;

    public Location(){
        this.storeClerkName = myLibrary.getNames();
    }
    public Location(String locationName, String personName){
        setStoreClerkName(personName);
        setStoreName(locationName);
    }
    public String getStoreClerkName() {
        return this.storeClerkName;
    }

    public void setStoreClerkName(String storeClerkName) {
        this.storeClerkName = storeClerkName;
    }
    public String getStoreName() {
        return this.storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
}
