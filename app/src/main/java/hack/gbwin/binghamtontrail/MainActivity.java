package hack.gbwin.binghamtontrail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.felipecsl.gifimageview.library.GifImageView;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements Serializable {

    MainPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*        GifImageView gifView = (GifImageView) findViewById(R.id.gifImageView);
        gifView.setBytes(bitmapData);
        GifImageView gifImageView;
        gifImageView.setOnFrameAvailable(new GifImageView.OnFrameAvailable() {
            @Override
            public Bitmap onFrameAvailable(Bitmap bitmap) {
                if (shouldBlur) {
                    return blur.blur(bitmap);
                }
                return bitmap;
            }
        });
       */
        Driver();
    }
    public void Driver() {
        String userName = "Kevin Wilson";
        player = new MainPlayer(userName);
        Home myRoom = new Home();
        player.setAlcohol(15);
        player.setIntelligence(150);
        player.setMoney(60.00);
        player.setHealth(85);
        player.setStudentStatus("Sophomore 2");
        player.setStamina(90);

        Person p1 = new Person("Mary Jane", 80);
        Person p2 = new Person("Jackson", 60);
        Person p3 = new Person("Cindy", 15);
        Person p4 = new Person("Linda", 50);
        Person p5 = new Person("Chris", 93);
        Person p6 = new Person("Ariel", 97);

        player.setCellPhone(p1);
        player.setCellPhone(p2);
        player.setCellPhone(p3);
        player.setCellPhone(p4);
        player.setCellPhone(p5);
        player.setCellPhone(p6);
    }

        //Print to screen: "You are a second semester sophomore named Wade Wilson who is unemployed, single, and it is the day before Valentine's Day."
        //print to Screen: "You want to find a date for tomorrow but you also need to study for the big test you have on Monday.
        //Print to screen: "What will you do?"

        





    public void onStartButtonPress(View view){
        Intent intent = new Intent(this,GameStart.class);
        intent.putExtra("myObject", player);
        startActivity(intent);
        finish();

    }
}
