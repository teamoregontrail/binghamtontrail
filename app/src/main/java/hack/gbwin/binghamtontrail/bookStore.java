package hack.gbwin.binghamtontrail;

import java.util.Hashtable;

/**
 * Created by kevintravers on 2/13/16.
 */
public class BookStore extends Location{
    private static Hashtable bookStore = new Hashtable();
    public BookStore(){
        super("Book Store",myLibrary.getNames());
        //setStoreName("Book Store");
        //setStoreClerkName(myLibrary.getNames());
        bookStore.put("Science TextBook", new Double(150.00));
        bookStore.put("Math TextBook",new Double(350.00));
        bookStore.put("History TextBook", new Double(250.00));
        bookStore.put("Philosphy TextBook", new Double(650.00));
    }

    public static Hashtable getBookStore() {
        return bookStore;
    }

}
